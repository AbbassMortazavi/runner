<?php

use App\Http\Controllers\TodoListController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('todo-list' , [\App\Http\Controllers\Api\v1\TodoListController::class , 'index'])->name('todo-list.store');

Route::prefix('v1')->namespace('Api')->group(function (){
    Route::get('todo-list' , [\App\Http\Controllers\Api\v1\TodoListController::class , 'index'])->name('todo-list.store');
});

Route::prefix('v2')->namespace('Api')->group(function (){
    Route::get('todo-list' , [\App\Http\Controllers\Api\v2\TodolistController::class , 'index'])->name('todo-list.store');
});






