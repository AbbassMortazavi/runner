<?php

use App\Http\Controllers\TodoListController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v2')->namespace('Api')->group(function (){
    Route::get('todo-list' , [\App\Http\Controllers\Api\v2\TodolistController::class , 'index'])->name('todo-list.store');
    Route::get('todo-list/{todolist}' , [\App\Http\Controllers\Api\v2\TodolistController::class , 'show'])->name('todo-list.show');
    Route::get('testV2' , [\App\Http\Controllers\Api\v2\TodolistController::class , 'testV2'])->name('testV2');
});






