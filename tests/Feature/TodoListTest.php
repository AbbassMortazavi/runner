<?php

namespace Tests\Feature;

use App\Models\TodoList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TodoListTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_fetch_all_todo_list()
    {
        //ta 7
       // $this->withoutExceptionHandling();
        //preperation /prepare
        $list = TodoList::factory()->create(['title'=>'my' , 'body'=>'ok']);

        //action / perform
        $response = $this->getJson(route('todo-list.store'));


        //assertion / predict
        $this->assertEquals(1 , count($response->json()));
        $this->assertEquals('my' , $response->json()['lists'][0]['title']);
    }

    public function test_fetch_single_todo_list()
    {
        //prepration
        $list = TodoList::factory()->create();


        //action
        $res = $this->getJson(route('todo-list.show' , $list->id))
        ->assertOk()
        ->json();


        //assertion

        dd($res );

        $this->assertEquals($res['title'] , $list->title);


    }
}
