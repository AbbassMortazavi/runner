<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

/**
 * Log an emergency message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_emergency(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->emergency($message);
    }
}

/**
 * Log an alert message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_alert(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->alert($message);
    }
}

/**
 * Log a critical message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_critical(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->critical($message);
    }else{
        return response()->json(['message'=>'log config is off']);
    }

}

/**
 * Log an error message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_error(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->error($message);
    }
}

/**
 * Log a warning message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_warning(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->warning($message);
    }
}

/**
 * Log a notice to the logs.
 *
 * @param string $message
 * @return void
 */
function log_notice(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->notice($message);
    }
}



/**
 * Log an informational message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_info(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->info($message);
    }
}

/**
 * Log a debug message to the logs.
 *
 * @param string $message
 * @return void
 */
function log_debug(string $message)
{
    if (config('logging.enable')) {
        Log::stack(['single', 'slack'])->debug($message);
    }
}


function log_info_db()
{
    if (config('logging.enable')) {
      //  \Illuminate\Support\Facades\Event::dispatch(new \App\Listeners\QueryExecutedListener());

        DB::listen(function($query){

            File::append(
                storage_path('/logs/query.log'),
                $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
            );
            $ok = $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL;
            dd($query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL);
        });

    }
}
