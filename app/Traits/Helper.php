<?php

namespace App\Traits;

use App\Events\FireLogSql;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

trait Helper
{
    public function logQuery()
    {
       /* DB::listen(function($query){
            File::append(
                storage_path('/logs/query.log'),
                $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
            );
            //dd($query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL);
        });

        Event::dispatch(new FireLogSql());*/

        \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {

            $sql = $query->sql;
            dd($sql);
            $time = $query->time;
            $connection = $query->connection->getName();

            Log::debug('query : '.$sql);
            Log::debug('time '.$time);
            Log::debug('connection '.$connection);
            Log::stack(['single', 'slack'])->info('query : '.$sql);
        });


    }
}
