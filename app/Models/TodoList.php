<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TodoList extends Model
{
  // use LogsActivity;
    use HasFactory;
    protected $fillable = [
        'title',
        'body'
    ];
    use LogsActivity;


    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['title'])
        ->logOnlyDirty();
        // Chain fluent methods for configuration options
    }

}
