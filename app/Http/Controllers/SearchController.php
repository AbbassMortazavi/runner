<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function test()
    {
        $u =User::search('Mr. Blake Ullrich')->get();
        return response()->json($u);
    }
}
