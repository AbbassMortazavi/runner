<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Models\TodoList;
use Illuminate\Http\Request;

class TodolistController extends Controller
{
    public function index()
    {
        //dd('v2');
        $lists = TodoList::all();
        return response(['lists'=>$lists]);
    }
    public function testV2()
    {
        dd('v2');
    }
    public function show(TodoList $todolist)
    {
       // $list = TodoList::findOrFail($todoList);

        return response($todolist);
    }
}
