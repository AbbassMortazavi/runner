<?php

namespace App\Listeners;

use App\Events\FireLogSql;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class LogSqlFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FireLogSql  $event
     * @return void
     */
    public function handle(FireLogSql $event)
    {
        /*DB::listen(function($query) {
           File::append(
               storage_path('/logs/query.log'),
               $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
           );
           dd($query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL);
           $test = $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL;
          });*/
        dd('kkkkkkkkkkkkkkkkkkkkkkk');
        Log::info(
            'SQL Query',
            [
                $event->sql,
                $event->bindings,
                $event->time,
            ]
        );

    }
}
