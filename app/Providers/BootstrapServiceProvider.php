<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Services\Helper\HeleprServiceServiceInterface::class, \App\Services\Helper\HeleprServiceService::class);
        $this->app->bind(\App\Services\Helper\HeleprServiceInterface::class, \App\Services\Helper\HelperService::class);
        //:end-bindings:
    }
}
