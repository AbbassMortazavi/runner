<?php

namespace App\Services\Helper;

use App\Events\FireLogSql;
use App\Services\Helper\HeleprServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;

class HelperService implements HeleprServiceInterface
{
    public function __construct()
    {
        //
    }
    public function logQuery()
    {
        DB::listen(function($query){
            File::append(
                storage_path('/logs/query.log'),
                $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
            );
            //dd($query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL);
        });

        Event::dispatch(new FireLogSql());


    }
}
